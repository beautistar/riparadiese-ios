//
//  R.swift
//  Riparadiese
//
//  Created by JIS on 3/18/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Riparadiese"
        static let SIGNUP = "Sign Up"
        
        static let OK = "Okay"
        static let CANCEL = "Cancel"
        static let ERROR = "Error Occured!"
        static let CONNECT_FAIL = "Connection to the server failed.\nPlease try again."
        
        static let AGREE_TERM = "To signup Riparadiese, \nYou have to agree to Terms & Privacy Policy"
        
        static let INPUT_EMAIL = "Please input email."
        static let INPUT_PWD = "Please input password. \nPassword must be more than 6 letters."
        static let CONFIRM_PWD = "Please confirm your password."
        static let SELECT_PHOTO = "Please select photo."
        static let INVALID_EMAIL = "Invalid Email address."
        static let INPUT_BIRTHDAY = "Please input your birthday."
        static let INPUT_USERNAME = "Please input username."
        
        static let INPUT_CAPTION = "Please input caption."
        static let INPUT_QUESTION = "Please answer the questions."
        

//        static let SELECT_PIC = "Select picture."
//        
        static let EMAIL_EXIST = "Email already exists."
        static let USERENAME_EXIST = "The username already exists."
        static let UPLOAD_FAIL = "Upload photo failed."
        static let USER_NOTEXIST = "The email is not registered."
        static let WRONG_PWD = "Invalid password."
//
//        
//        
//        
//        static let INPUT_CODE = "Please input code."
//        
//        static let INPUT_LASTNAME = "Please input lastname."


//        static let CHILD_NOTEXIST = "The code is not registered."
//        static let GENERATE_CODE = "Please generate child code."
//        static let SEND_CHILDCODE = "Success to save child.\nPlease send code to your child."
//        static let INPUT_CHORENAME = "Please input chore name."
//        static let SELECT_CHILD = "Please select child."
//        static let SELECT_CHORE = "Please select a chore."
//        static let SELECT_PRICE = "Please select a price."
//        static let SUCCESS_SAVE = "Successfuly saved."
//        static let MAX_IMAGE_6 = "Please select maximum 6 images"
//        static let SUCCESS_PAID = "Successfuly paid."
//        
        static let PHOTO_SOURCE = "Photo Source"
        static let CAMERA = "Camera"
        static let PHOTO_ALBUMS = "Photo Albums"
//
//        static let CHORE_STATE = ["Working", "Completed", "Checked", "Paid", "Declined"]
    }
    
    
}

