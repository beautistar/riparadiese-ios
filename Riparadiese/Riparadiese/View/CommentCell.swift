//
//  CommentCell.swift
//  Riparadise
//
//  Created by JIS on 3/22/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var imvCommentOwnerPhoto: UIImageView!
    @IBOutlet weak var lblCommentOwnerName: UILabel!
    @IBOutlet weak var lblCommentedTime: UILabel!
    @IBOutlet weak var lblCommentContent: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
