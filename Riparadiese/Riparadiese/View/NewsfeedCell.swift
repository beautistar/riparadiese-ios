//
//  NewsfeedView.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class NewsfeedCell: UICollectionViewCell {
    
    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblQuestion1: UILabel!
    @IBOutlet weak var lblQuestion2: UILabel!
    @IBOutlet weak var tvCaption: UITextView!
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var imvLike1: UIImageView!
    @IBOutlet weak var imvLike2: UIImageView!
    

//    override func draw(_ rect: CGRect) {
//        
//    }    
//    
//    override init (frame: CGRect) {
//        super.init(frame : frame)
//
//    }
//    
//    convenience init () {
//        self.init(frame:CGRect.zero)
//
//    }
//    
//    override func prepareForInterfaceBuilder() {}
//    
//    override func awakeFromNib() {
//        imvUserPhoto.layer.borderWidth = 1.0
//        imvUserPhoto.layer.borderColor = Constants.mainColor.cgColor
//        
////        imvPicture.layer.borderWidth = 1.0
////        imvPicture.layer.borderColor = Constants.mainColor.cgColor
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
}
