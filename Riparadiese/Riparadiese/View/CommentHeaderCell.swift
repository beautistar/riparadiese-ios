//
//  CommentHeaderCell.swift
//  Riparadise
//
//  Created by JIS on 3/22/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class CommentHeaderCell: UITableViewCell {

    @IBOutlet weak var imvPostOwnerPhoto: UIImageView!
    @IBOutlet weak var lblPostOwnerName: UILabel!
    @IBOutlet weak var lblPostedTime: UILabel!
    @IBOutlet weak var imvPost: UIImageView!
    @IBOutlet weak var tvCaption: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
