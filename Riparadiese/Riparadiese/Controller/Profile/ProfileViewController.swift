//
//  ProfileViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material
import SDWebImage
import Alamofire

class ProfileViewController: BaseViewController, UITextFieldDelegate,  UICollectionViewDataSource, UICollectionViewDelegate, UITabBarDelegate, UITabBarControllerDelegate {

    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var cvNewsfeed: UICollectionView!
    
    
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    var _user:UserEntity!
    var _newsfeeds = [NewsfeedEntity]()
    var _currentIndexPath:Int=0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if FromNotification == 0 {
            
            //self.navigationItem.leftBarButtonItem?.isEnabled = false
            _user = AppDelegate.getUser()
            
        } else {
            FromNotification = 0
            //self.navigationItem.leftBarButtonItem?.isEnabled = true
        }
        
        self.initUI()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    func initUI() {
        
        w = self.view.frame.size.width
        h = self.view.frame.size.height - 64 - 50 - 70
        
        imvProfile.layer.borderColor = Constants.mainColor.cgColor
        imvProfile.layer.borderWidth = 1.0
        
        self.imvProfile.sd_setImage(with: URL(string:_user._profileUrl), placeholderImage: UIImage(named: "img_user"))
        self.lblName.text = _user._username
        
        //self.getPost()
        self.getMyNewsfeeds()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func getMyNewsfeeds() {
        
        self._newsfeeds.removeAll()
        
        for index in 0 ..< Newsfeeds.count {
            
            if Newsfeeds[index]._user_id == _user._id {
                
                self._newsfeeds.append(Newsfeeds[index])
            }
        }
        
        self.cvNewsfeed.reloadData()
    }
    
    
    // MARK: collection view datasource
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _newsfeeds.count
        
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let feed = _newsfeeds[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsfeedCell", for: indexPath) as! NewsfeedCell
        
        cell.imvUserPhoto.sd_setImage(with: URL(string:feed._photo_url), placeholderImage: UIImage(named: "img_user"))
        cell.lblUserName.text = feed._user_name
        //cell.lblQuestion1.text = feed._question1
        //cell.lblQuestion2.text = feed._question2
        cell.tvCaption.text = feed._caption
        cell.imvPicture.sd_setImage(with: URL(string:feed._file_url), placeholderImage:UIImage(named : "ic_attach"))
        
        if feed._likeCount > 0 {
            
            
        }
        
        if feed._recentLikeType != -1 && feed._likeCount > 0 {
            
                cell.imvLike1.image = UIImage(named: Constants.like_icons[feed._recentLikeType])
                cell.imvLike1.frame.size.width = 20
                cell.imvLike1.isHidden = false
            
        } else {
            cell.imvLike1.frame.size.width = 0
            cell.imvLike1.isHidden = true
        }
        
        if feed._myLikeType != -1 && feed._likeCount > 0 {
            if feed._recentLikeType != feed._myLikeType {
                cell.imvLike2.image = UIImage(named: Constants.like_icons[feed._myLikeType])
                cell.imvLike2.frame.size.width = 20
                cell.imvLike2.isHidden = false
            } else {
                cell.imvLike2.frame.size.width = 0
                cell.imvLike2.isHidden = true
            }
        } else {
            cell.imvLike2.frame.size.width = 0
            cell.imvLike2.isHidden = true
        }
        
        if feed._likeCount > 0 {
            if feed._myLikeType != -1 {
                cell.lblLike.text = "You and " + String(feed._likeCount-1) + " others"
                
            } else {
                cell.lblLike.text = String(feed._likeCount) + " likes"
            }
        } else {
            cell.lblLike.text = "0 like"
        }
        
        if feed._commentCount == 0 {
            cell.lblComment.text = "0 comments"
        } else {
            cell.lblComment.text = String(feed._commentCount) + " comments"
        }
        
        if feed._shareCount == 0 {
            cell.lblShare.text = "0 shares"
        } else {
            cell.lblShare.text = String(feed._shareCount) + " shares"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let feed = self._newsfeeds[indexPath.row];
        
        let profileDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileDetailViewController") as! ProfileDetailViewController
        profileDetailVC._currentFeed = feed
        //profileDetailVC._currentIndex = indexPath.row
        //        self.present(profileDetailVC, animated: false, completion: nil)
        self.navigationController?.pushViewController(profileDetailVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        _currentIndexPath = Int(self.cvNewsfeed.contentOffset.y / self.cvNewsfeed.frame.size.height)
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        
            let mainNav = self.storyboard?.instantiateViewController(withIdentifier: "MainNav") as! UINavigationController
            
            UIApplication.shared.keyWindow?.rootViewController = mainNav
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
}

extension ProfileViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(w), height: CGFloat(h))
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
    //3
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        insetForSectionAt section: Int) -> UIEdgeInsets {
//        return sectionInsets
//    }
//    
//    // 4
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return sectionInsets.left
//    }
}
