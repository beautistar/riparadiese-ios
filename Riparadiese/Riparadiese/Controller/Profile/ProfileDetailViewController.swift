//
//  ProfileDetailViewController.swift
//  Riparadise
//
//  Created by JIS on 4/5/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import EMEmojiableBtn
import Alamofire

class ProfileDetailViewController: BaseViewController, EMEmojiableBtnDelegate {
    
    @IBOutlet weak var imvUserPhoto: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAnswer1: UILabel!
    @IBOutlet weak var lblAnswer2: UILabel!
    @IBOutlet weak var tvCaption: UITextView!
    @IBOutlet weak var imvPost: UIImageView!
    @IBOutlet weak var imvRecentLike: UIImageView!
    @IBOutlet weak var imvMyLike: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var lblCommentCount: UILabel!
    @IBOutlet weak var lblShareCount: UILabel!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var likeView: UIView!
    
    
    var _currentFeed:NewsfeedEntity!
    //var _currentIndex:Int=0
    var w:CGFloat = 0
    var h:CGFloat = 0
    var btn:EMEmojiableBtn!
    var dataArray = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initView()
    }
    
    func initView() {
        
        self.tabBarController?.tabBar.isHidden = false
        
        self.title = _currentFeed._caption
        print(_currentFeed._caption)
        
        // button image color chage
        // comment button
        let origCommentImage = UIImage(named: "ic_comment")
        
        let tintedCommentImage = origCommentImage?.withRenderingMode(.alwaysTemplate)
        
        btnComment.setImage(tintedCommentImage, for: UIControlState.normal)
        btnComment.tintColor = Constants.darkGrayColor
        
        // share button
        let originShareImage = UIImage(named: "ic_share")
        let tintedShareImage = originShareImage?.withRenderingMode(.alwaysTemplate)
        btnShare.setImage(tintedShareImage, for: UIControlState.normal)
        btnShare.tintColor = Constants.darkGrayColor
        
        
        // init EmEmoji View
        
        let config = EMEmojiableBtnConfig.init()
        //config.spacing = 6.0
        config.size = 50.0
        config.minSize = 40.0
        config.maxSize = 90.0
        config.s_options_selector = -20.0
        config.informationViewBackgroundColor = UIColor.clear
        //config.optionsViewShadowOffset = CGSize(width:20, height:20)
        config.optionsViewShadowColor = UIColor.clear
        config.optionsViewInitialAlpha = 0.0
        
        btn = EMEmojiableBtn.init(frame: CGRect(x:0, y:5, width:30, height:30), with: config)
        btn.delegate = self
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        btn.backgroundColor = UIColor.init(red: 240, green: 240, blue: 240)
        btn.setImage(UIImage.init(named: "ic_gold_ring"), for: UIControlState.normal)
        
        btn.layer.cornerRadius = 15
        btn.setTitle("Like", for: .normal)
        
        dataArray = [
            
            EMEmojiableOption.init(image: "ic_rip", withName: "RIP"),
            EMEmojiableOption.init(image: "ic_gold_ring", withName: "Like"),
            EMEmojiableOption.init(image: "ic_candle", withName: "Fire"),
            EMEmojiableOption.init(image: "ic_heart", withName: "Love"),
            EMEmojiableOption.init(image: "ic_bird", withName: "Fly"),
            EMEmojiableOption.init(image: "ic_rose", withName: "Hmm"),
        ]
        
        btn.dataset = dataArray
        self.likeView.addSubview(btn)
        
        ////////////////////
        
        
        imvUserPhoto.sd_setImage(with: URL(string:_currentFeed._photo_url), placeholderImage: UIImage(named: "img_user"))
        lblUserName.text = _currentFeed._user_name
        lblAnswer1.text = _currentFeed._question1
        lblAnswer2.text = _currentFeed._question2
        tvCaption.text = _currentFeed._caption
        imvPost.sd_setImage(with: URL(string:_currentFeed._file_url), placeholderImage:UIImage(named : "ic_attach"))
        
        if _currentFeed._recentLikeType != -1 && _currentFeed._likeCount > 0 {
            
                imvRecentLike.image = UIImage(named: Constants.like_icons[_currentFeed._recentLikeType])
                imvRecentLike.frame.size.width = 20
                imvRecentLike.isHidden = false
            
        } else {
            imvRecentLike.frame.size.width = 0
            imvRecentLike.isHidden = true
        }
        
        if _currentFeed._myLikeType != -1 && _currentFeed._likeCount > 0 {
            if _currentFeed._recentLikeType != _currentFeed._myLikeType {
                imvMyLike.image = UIImage(named: Constants.like_icons[_currentFeed._myLikeType])
                imvMyLike.frame.size.width = 20
                imvMyLike.isHidden = false
            } else {
                imvMyLike.frame.size.width = 0
                imvMyLike.isHidden = true
            }
        } else {
            imvMyLike.frame.size.width = 0
            imvMyLike.isHidden = true
        }
        
        if _currentFeed._likeCount != 0 {
            if _currentFeed._myLikeType != -1 {
                lblLikeCount.text = "You and " + String(_currentFeed._likeCount-1) + " others"

            } else {
                lblLikeCount.text = String(_currentFeed._likeCount) + " likes"
            }
        } else {
            lblLikeCount.text = "0 like"
        }
        
        if _currentFeed._commentCount == 0 {
            lblCommentCount.text = "0 comments"
        } else {
            lblCommentCount.text = String(_currentFeed._commentCount) + " comments"
        }
        
        if _currentFeed._shareCount == 0 {
            lblShareCount.text = "0 shares"
        } else {
            lblShareCount.text = String(_currentFeed._shareCount) + " shares"
        }

    }
    
    @IBAction func gotoFullView(_ sender: Any) {
        
        let photoVC = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        photoVC._photoUrl = _currentFeed._file_url
        self.present(photoVC, animated: false, completion: nil)
        
    }
    
    
    @IBAction func commentAction(_ sender: Any) {
        
        let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        commentVC._selectedFeed = _currentFeed
        self.navigationController?.pushViewController(commentVC, animated: true)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
    
        _ = navigationController?.popViewController(animated: false)
    
    }
    
    func setLike(_ _type:Int) {
        
        print(_currentFeed._caption)
        
        lblLikeCount.text = String(_currentFeed._likeCount+1) + "likes"
        imvMyLike.image = UIImage(named: Constants.like_icons[_type])
        imvMyLike.frame.size.width = 20
        imvMyLike.isHidden = false
        
        
        //showLoadingView()
        self.showToast("liking...")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //for test
                
                multipartFormData.append("\(self._currentFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(_type)".data(using:String.Encoding.utf8)!, withName: "type")
                
        },
            to: Constants.REQ_SETLIKE,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("set emoji like :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                if (self._currentFeed._myLikeType == -1) {
                                    
                                    for i in 0 ..< Newsfeeds.count {
                                        
                                        if Newsfeeds[i]._id == self._currentFeed._id {
                                            
                                            if Newsfeeds[i]._myLikeType != -1 {
                                                Newsfeeds[i]._likeCount += 1
                                            }
                                            Newsfeeds[i]._myLikeType = _type
                                        }
                                    }
                                }
                                
                                //  self.hideLoadingView()
                                self.showToast("like succeed")
                            }
                            
                        } else  {
                            
                            self.showToast("like failed")
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    //self.hideLoadingView()
                    return
                }
        }
        )
    }
    
    func setShare() {
        
        //showLoadingView()
        self.showToast("sharing...")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //for test
                multipartFormData.append("\(self._currentFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                
        },
            to: Constants.REQ_SETSHARE,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("set emoji like :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                
                                for index in 0 ..< Newsfeeds.count {
                                    
                                    if Newsfeeds[index]._id == self._currentFeed._id {
                                        
                                        Newsfeeds[index]._shareCount += 1
                                    }
                                    break
                                }
                                
                                //self.hideLoadingView()
                                self.showToast("sharing succeed")
                                
                            }
                            
                        } else  {
                            
                            self.showToast("sharing failed")
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    @IBAction func shareAction(_ sender: Any) {
    
        detailViewed = true
        
        let imageView = UIImageView.init(frame: CGRect(x: 2, y: 2, width: 100, height: 100))
        imageView.sd_setImage(with: URL(string:_currentFeed._file_url), placeholderImage:UIImage(named : "ic_attach"))
        
        
        let imvUserPhoto = UIImageView.init(frame: CGRect(x: 2, y: 104, width: 30, height: 30))
        imvUserPhoto.sd_setImage(with: URL(string:_currentFeed._photo_url), placeholderImage:UIImage(named : "ic_attach"))
        
        let shareView = UIView.init(frame: CGRect(x: 0, y: 0, width: 104, height: 135))
        shareView.backgroundColor = Constants.grayColor
        
        let lblName = UILabel.init(frame: CGRect(x:34, y:104, width:65, height:30))
        lblName.text = _currentFeed._user_name
        lblName.font = UIFont.systemFont(ofSize: 8)
        lblName.contentMode = UIViewContentMode.top
        lblName.textColor = Constants.mainColor
        lblName.numberOfLines = 0
        
        shareView.addSubview(imageView)
        shareView.addSubview(imvUserPhoto)
        shareView.addSubview(lblName)
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 104, height: 140), false, 0);
        shareView.drawHierarchy(in: CGRect(x: 0, y: 0, width: 104, height: 140), afterScreenUpdates: true)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        
        let vc = UIActivityViewController(activityItems: [image, "Help " + _currentFeed._user_name + " say r.i.p by downloading Riparadise.", Constants.APP_URL], applicationActivities: [])
        
        present(vc, animated: true, completion: nil)
        
        vc.completionWithItemsHandler = { activity, success, items, error in
        print("activity: \(activity), success: \(success), items: \(items), error: \(error)")
        
        if success {
            self.setShare()
        }
    }
}

override func motionCancelled(_ motion: UIEventSubtype, with event: UIEvent?) {
    print("Cancelled")
}

func emEmojiableBtnSingleTap(_ button: EMEmojiableBtn) {
    print("Single tap")
    
}

func emEmojiableBtn(_ button: EMEmojiableBtn, selectedOption index: UInt) {
    let btnOption = dataArray[Int(index)] as! EMEmojiableOption
    button.setImage(UIImage.init(named: btnOption.imageName), for: UIControlState.normal)
    
    print(index)
    
    self.setLike(Int(index))
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
