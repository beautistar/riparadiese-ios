//
//  PostViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material
import KMPlaceholderTextView
import Alamofire

class PostViewController: BaseViewController, UITextViewDelegate, UITabBarControllerDelegate, UITabBarDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

   
    @IBOutlet weak var imvPicture: UIImageView!
    @IBOutlet weak var tvCaption: KMPlaceholderTextView!
    @IBOutlet weak var tfQuestion1: UITextField!
    @IBOutlet weak var tfQuestion2: UITextField!
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgProfile : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()        

        // Do any additional setup after loading the view.
        self._picker.delegate = self
        _picker.allowsEditing = true
        
        self.initUI()
        self.doTakePhoto()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)       
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        if tabBarController.selectedIndex == 2 {
            doTakePhoto()
        }
    }
    
    
    func initUI() {
        
        imvPicture.layer.borderColor = Constants.mainColor.cgColor
        imvPicture.layer.borderWidth = 1.0
        
        tvCaption.text = ""
        tvCaption.layer.cornerRadius = 5
        tvCaption.layer.borderColor = Constants.grayColor.cgColor
        tvCaption.layer.borderWidth = 1
        tfQuestion1.text = ""
        tfQuestion2.text = ""
        _imgProfile = ""
        self.imvPicture.image = UIImage.init(named: "ic_attach")
    }    

  
    @IBAction func attachAction(_ sender: Any) {
        doTakePhoto()
    }
    
    @IBAction func postAction(_ sender: Any) {
        
        if self.isValid() {
            
            self.doPost()
        }
        
    }
    
    func isValid() -> Bool {
        
        if tvCaption.text == nil || tvCaption.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_CAPTION, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfQuestion1.text == nil || tfQuestion1.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_QUESTION, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfQuestion2.text == nil || tfQuestion2.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_QUESTION, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
        
    }
    
    
    func doPost() {
        
        showLoadingView()
        
        let caption = tvCaption.text!
        let question1 = tfQuestion1.text!
        let question2 = tfQuestion2.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath:self._imgProfile!), withName: "file")
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(caption)".data(using:String.Encoding.utf8)!, withName: "caption")
                multipartFormData.append("\(question1)".data(using:String.Encoding.utf8)!, withName: "question1")
                multipartFormData.append("\(question2)".data(using:String.Encoding.utf8)!, withName: "question2")
        },
            to: Constants.REQ_POST,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("post response : ", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                self.initUI()
                                
                            } else if result_code == 205 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    
    
    
    func doTakePhoto() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction: UIAlertAction = UIAlertAction(title: R.string.CAMERA, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
        else
        {
            _picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imvPicture.image = pickedImage
            _imgProfile = saveToFile(image: pickedImage, filePath: Constants.SAVE_ROOT_PATH, fileName: "temp.png")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let currentCharacterCount = textView.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        
        
        let newLength = currentCharacterCount + text.characters.count - range.length
        return newLength <= 140
    }
    
    

    
}
