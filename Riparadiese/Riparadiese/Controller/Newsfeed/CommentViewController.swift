//
//  CommentViewController.swift
//  Riparadise
//
//  Created by JIS on 3/22/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Alamofire


class CommentViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var tblComment: UITableView!
    @IBOutlet weak var tvComment: UITextView!

    var tapGesture:UITapGestureRecognizer!
    @IBOutlet weak var messageContainerConstrain: NSLayoutConstraint!
    
    var _selectedFeed:NewsfeedEntity!
    var _comments = [CommentEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblComment.tableFooterView = UIView()
        
        self.tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tableViewDidTap(_:)))
        self.tapGesture.numberOfTouchesRequired = 1
        self.tapGesture.numberOfTapsRequired = 1
        self.tblComment.addGestureRecognizer(tapGesture)
        
        tblComment.estimatedRowHeight = 70
        tblComment.rowHeight = UITableViewAutomaticDimension
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.getComment()
        
        detailViewed = true

    }
    
    func setCurrentFeed() {        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tvComment.becomeFirstResponder()
    }
   
    func tableViewDidTap(_ gesture:UITapGestureRecognizer) {
        
        self.tvComment.resignFirstResponder()
    }
    
    
    func getComment() {
        
        showLoadingView()
        
        //showToast("Commiting...")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                // for test
                multipartFormData.append("\(self._selectedFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")
                
        },
            to: Constants.REQ_GETCOMMENT,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("get comment :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                //self.showToast("Commit succeed")
                                
                                self._comments.removeAll()
                                
                                let comments = JSON[Constants.RES_COMMENTS] as! [NSDictionary]
                                
                                for index in 0 ..< comments.count {
                                    
                                    let _comment = CommentEntity()
                                    _comment._id = Int(comments[index][Constants.RES_ID] as! String)!
                                    _comment._owner._id = Int(comments[index][Constants.RES_USERID] as! String)!
                                    _comment._owner._username = comments[index][Constants.RES_USERNAME] as! String
                                    _comment._owner._profileUrl = comments[index][Constants.RES_PHOTOURL] as! String
                                    _comment._comment = comments[index][Constants.RES_COMMENT] as! String
                                    _comment._time = comments[index][Constants.RES_REGDATE] as! String
                                    
                                    self._comments.append(_comment)
                                    
                                }
                                
                                self.tblComment.reloadData()
                            }
                            
                        } else  {
                            
                            //self.showToast("Commit failed")
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
 
    
    func setComment() {
        
        showToast("Commiting...")
//        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //                for test
                multipartFormData.append("\(self._selectedFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(self.tvComment.text!)".data(using:String.Encoding.utf8)!, withName: "comment")
                
        },
            to: Constants.REQ_SETCOMMENT,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("set comment :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let new_comment = CommentEntity()
                                new_comment._id = Int(JSON[Constants.RES_ID] as! NSNumber)
                                new_comment._owner = AppDelegate.getUser()
                                new_comment._comment = self.tvComment.text
                                new_comment._time = "Just now"
                                
                                self._comments.append(new_comment)
                                
                                self.tblComment.reloadData()
                                
                                self.tvComment.resignFirstResponder()
                                self.tvComment.text = ""
                                self.showToast("Commit succeed")
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showToast("Commit failed")
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    // MARK: TableView datasource & Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return _comments.count  + 1
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            
            return 70 + self.view.bounds.width
        }
        
        return UITableViewAutomaticDimension
    }
    
    @IBAction func postAction(_ sender: Any) {
        
        if tvComment.text != nil || tvComment.text?.characters.count != 0 {
            
            self.setComment()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let headerCell = tableView.dequeueReusableCell(withIdentifier: "CommentHeaderCell") as! CommentHeaderCell
            headerCell.selectionStyle = UITableViewCellSelectionStyle.none

            headerCell.imvPostOwnerPhoto.sd_setImage(with: URL(string:_selectedFeed._photo_url), placeholderImage: UIImage(named: "img_user"))
            headerCell.lblPostOwnerName.text = _selectedFeed._user_name
            headerCell.lblPostedTime.text = _selectedFeed._time
            headerCell.tvCaption.text = _selectedFeed._caption
            headerCell.imvPost.sd_setImage(with: URL(string:_selectedFeed._file_url), placeholderImage: UIImage(named: "ic_attach"))
            
            return headerCell
            
        }
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let comment = _comments[indexPath.row-1]
        
        cell.imvCommentOwnerPhoto.sd_setImage(with: URL(string:comment._owner._profileUrl), placeholderImage: UIImage(named: "img_user"))
        cell.lblCommentOwnerName.text = comment._owner._username
        cell.lblCommentContent.text = comment._comment
        cell.lblCommentedTime.text = comment._time
        
        return cell
        
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let item = _items[indexPath.row]
//        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "ResEditViewController") as! ResEditViewController
//        detailVC.selectedItem = item;
//        
//        self.navigationController?.pushViewController(detailVC, animated: true)
        
//        [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        
        
    }
    
//    func zoomToSelectedImage(_ cell:UICollectionViewCell) {
//    
//        UIImageView *zoomImage = [[UIImageView alloc] initWithImage:[self.myPictures objectAtIndex:indexPath.row]];
//        
//        zoomImage.contentMode = UIViewContentModeScaleAspectFit;
//        
//        CGRect zoomFrameTo = CGRectMake(0,0, self.view.frame.size.width,self.view.frame.size.height);
//        
//        UICollectionView *cv = (UICollectionView *)[self.view viewWithTag:66]; // Change to whatever the tag value of your collectionView is
//        
//        cv.hidden = TRUE;
//        
//        UICollectionViewCell *cellToZoom =(UICollectionViewCell *)[cv cellForItemAtIndexPath:indexPath];
//        
//        CGRect zoomFrameFrom = cellToZoom.frame;
//        
//        [self.view addSubview:zoomImage];
//        
//        zoomImage.frame = zoomFrameFrom;
//        
//        zoomImage.alpha = 0.2;
//        
//        [UIView animateWithDuration:0.2 animations:
//        
//        ^{
//        
//        zoomImage.frame = zoomFrameTo;
//        
//        zoomImage.alpha = 1;
//        
//        } completion:nil];
//        
//    }


}
