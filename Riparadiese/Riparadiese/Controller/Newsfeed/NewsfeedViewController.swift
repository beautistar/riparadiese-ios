//
//  NewsfeedViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import EMEmojiableBtn
import Alamofire

class NewsfeedViewController: BaseViewController, UIScrollViewDelegate, EMEmojiableBtnDelegate, UITabBarControllerDelegate, UITabBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    var viewHeight:CGFloat=0.0
    var _currentFeed:NewsfeedEntity!
    var _currentIndexPath:Int = 0
    
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var cvNewsfeed: UICollectionView!

    var btn:EMEmojiableBtn!
    var dataArray = [AnyObject]()
    var _newsfeeds = [NewsfeedEntity]()
    
    //var currentewsfeedView:NewsfeedView!
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    
    @IBOutlet weak var scroll: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tabBar = UIApplication.shared.keyWindow?.rootViewController as! UITabBarController
        tabBar.delegate = self
        detailViewed = false
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.initUI()
        
        self.tabBarController?.tabBar.isHidden = false
        if detailViewed == false {
            self.getNewsfeed()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //detailViewed = false
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if tabBarController.selectedIndex != 0 {
            
            detailViewed = false            
        }
    }
    
    func initUI() {
        
        w = self.view.frame.size.width
        h = self.view.frame.size.height - 64 - 50 - 70
        
        // button image color chage
        // comment button
        let origCommentImage = UIImage(named: "ic_comment")
        
        let tintedCommentImage = origCommentImage?.withRenderingMode(.alwaysTemplate)
        
        btnComment.setImage(tintedCommentImage, for: UIControlState.normal)
        btnComment.tintColor = Constants.darkGrayColor
        
         // share button
        let originShareImage = UIImage(named: "ic_share")
        let tintedShareImage = originShareImage?.withRenderingMode(.alwaysTemplate)
        btnShare.setImage(tintedShareImage, for: UIControlState.normal)
        btnShare.tintColor = Constants.darkGrayColor
        

        // init EmEmoji View
        
        let config = EMEmojiableBtnConfig.init()
        //config.spacing = 6.0
        config.size = 50.0
        config.minSize = 40.0
        config.maxSize = 90.0
        config.s_options_selector = -20.0
        config.informationViewBackgroundColor = UIColor.clear
        //config.optionsViewShadowOffset = CGSize(width:20, height:20)
        config.optionsViewShadowColor = UIColor.clear
        config.optionsViewInitialAlpha = 0.0
        
        btn = EMEmojiableBtn.init(frame: CGRect(x:0, y:5, width:30, height:30), with: config)
        btn.delegate = self
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 11)
        btn.backgroundColor = UIColor.init(red: 240, green: 240, blue: 240)
        btn.setImage(UIImage.init(named: "ic_gold_ring"), for: UIControlState.normal)
        
        btn.layer.cornerRadius = 15
        btn.setTitle("Like", for: .normal)
        
        dataArray = [
            
            EMEmojiableOption.init(image: "ic_rip", withName: "RIP"),
            EMEmojiableOption.init(image: "ic_gold_ring", withName: "Like"),
            EMEmojiableOption.init(image: "ic_candle", withName: "Fire"),
            EMEmojiableOption.init(image: "ic_heart", withName: "Love"),
            EMEmojiableOption.init(image: "ic_bird", withName: "Fly"),
            EMEmojiableOption.init(image: "ic_rose", withName: "Hmm"),
            ]
        
        btn.dataset = dataArray
        self.likeView.addSubview(btn)
    }
    
    @IBAction func shareAction(_ sender: Any) {
        
        detailViewed = true
        
        _currentFeed = Newsfeeds[_currentIndexPath]
        
        let imageView = UIImageView.init(frame: CGRect(x: 2, y: 2, width: 100, height: 100))
        imageView.sd_setImage(with: URL(string:_currentFeed._file_url), placeholderImage:UIImage(named : "ic_attach"))
        
        
        let imvUserPhoto = UIImageView.init(frame: CGRect(x: 2, y: 104, width: 30, height: 30))
        imvUserPhoto.sd_setImage(with: URL(string:_currentFeed._photo_url), placeholderImage:UIImage(named : "ic_attach"))
       
        let shareView = UIView.init(frame: CGRect(x: 0, y: 0, width: 104, height: 135))
        shareView.backgroundColor = Constants.grayColor
        
        let lblName = UILabel.init(frame: CGRect(x:34, y:104, width:65, height:30))
        lblName.text = _currentFeed._user_name
        lblName.font = UIFont.systemFont(ofSize: 8)
        lblName.contentMode = UIViewContentMode.top
        lblName.textColor = Constants.mainColor
        lblName.numberOfLines = 0
        
        shareView.addSubview(imageView)
        shareView.addSubview(imvUserPhoto)
        shareView.addSubview(lblName)
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 104, height: 140), false, 0);
        shareView.drawHierarchy(in: CGRect(x: 0, y: 0, width: 104, height: 140), afterScreenUpdates: true)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;        
        
        let vc = UIActivityViewController(activityItems: [image, "Help " + _currentFeed._user_name + " say r.i.p by downloading Riparadise.", Constants.APP_URL], applicationActivities: [])
        
        present(vc, animated: true, completion: nil)
        
        vc.completionWithItemsHandler = { activity, success, items, error in
            print("activity: \(activity), success: \(success), items: \(items), error: \(error)")
            
            
            
            if success {
                self.setShare()
            }
        }
    }
    
    override func motionCancelled(_ motion: UIEventSubtype, with event: UIEvent?) {
        print("Cancelled")
    }
    
    func emEmojiableBtnSingleTap(_ button: EMEmojiableBtn) {
        print("Single tap")
     
    }
    
    func emEmojiableBtn(_ button: EMEmojiableBtn, selectedOption index: UInt) {
        let btnOption = dataArray[Int(index)] as! EMEmojiableOption
        button.setImage(UIImage.init(named: btnOption.imageName), for: UIControlState.normal)
        
        print(index)
        
        self.setLike(Int(index))
    }
    
    @IBAction func commentAction(_ sender: Any) {
        
        self.gotoComment()
    }
    
    func gotoComment() {
        
        _currentFeed = _newsfeeds[_currentIndexPath]
        
        let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        commentVC._selectedFeed = _currentFeed
        self.navigationController?.pushViewController(commentVC, animated: true)
    }
    
    func getNewsfeed() {
        
        showLoadingView()
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(1)".data(using:String.Encoding.utf8)!, withName: "page_index")
        },
            to: Constants.REQ_NEWSFEED,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("result :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                self._newsfeeds.removeAll()
                                Newsfeeds.removeAll()
                                
                                let newsfeeds = JSON[Constants.RES_NEWSFEED] as! [NSDictionary]
                                
                                for index in 0 ..< newsfeeds.count {
                                    
                                    let feed = NewsfeedEntity()
                                    feed._id = Int(newsfeeds[index][Constants.RES_ID] as! String)!
                                    feed._user_id = Int(newsfeeds[index][Constants.RES_USERID] as! String)!
                                    feed._user_name = newsfeeds[index][Constants.RES_USERNAME] as! String
                                    feed._photo_url = newsfeeds[index][Constants.RES_PHOTOURL] as! String
                                    feed._caption = newsfeeds[index][Constants.RES_CAPTION] as! String
                                    feed._question1 = newsfeeds[index][Constants.RES_QUESTION1] as! String
                                    feed._question2 = newsfeeds[index][Constants.RES_QUESTION2] as! String
                                    feed._file_url = newsfeeds[index][Constants.RES_FILEURL] as! String
                                    feed._time = newsfeeds[index][Constants.RES_REGDATE] as! String
                                    
                                    if (newsfeeds[index][Constants.RES_MYLIKETYPE] as? String) != nil {
                                        feed._myLikeType = Int(newsfeeds[index][Constants.RES_MYLIKETYPE] as! String)!
                                    } else {
                                        feed._myLikeType = newsfeeds[index][Constants.RES_MYLIKETYPE] as! Int
                                    }
                                    
                                    if (newsfeeds[index][Constants.RES_RECENTLIKETYPE] as? String) != nil {
                                        feed._recentLikeType = Int(newsfeeds[index][Constants.RES_RECENTLIKETYPE] as! String)!
                                    } else {
                                        feed._recentLikeType = newsfeeds[index][Constants.RES_RECENTLIKETYPE] as! Int
                                    }

                                    feed._shareCount = Int(newsfeeds[index][Constants.RES_SHARECOUNT] as! NSNumber)
                                    feed._commentCount = Int(newsfeeds[index][Constants.RES_COMMENTCOUNT] as! NSNumber)
                                    feed._likeCount = Int(newsfeeds[index][Constants.RES_LIKECOUNT] as! NSNumber)
  
                                    Newsfeeds.append(feed)
                                    
                                    if feed._likeCount + feed._shareCount + feed._commentCount > 9 {
                                        self._newsfeeds.append(feed)
                                    }
                                    
                                }
                                
                                if self._newsfeeds.count == 0 {
                                    
                                    self.btn.isEnabled = false
                                    self.btnShare.isEnabled = false
                                    self.btnComment.isEnabled = false
                                    
                                    self.cvNewsfeed.isHidden = true
                                }
                                
                                self.cvNewsfeed.reloadData()
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    func setLike(_ _type:Int) {
        
        _currentFeed = _newsfeeds[_currentIndexPath]
        
        print(_currentFeed._caption)
        
        if (_currentFeed._myLikeType == -1) {
            self._newsfeeds[_currentIndexPath]._likeCount += 1
        }
        self._newsfeeds[_currentIndexPath]._myLikeType = _type
        
        self.cvNewsfeed.reloadData()
        
        //showLoadingView()
        self.showToast("liking...")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //for test
     
                multipartFormData.append("\(self._currentFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append("\(_type)".data(using:String.Encoding.utf8)!, withName: "type")
                
        },
            to: Constants.REQ_SETLIKE,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("set emoji like :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                //  self.hideLoadingView()
                                self.showToast("like succeed")
                            }
                            
                        } else  {
                            
                            self.showToast("like failed")
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    //self.hideLoadingView()
                    return
                }
        }
        )
    }
    
    func setShare() {
        
        //showLoadingView()
        self.showToast("sharing...")
        
        _currentFeed = _newsfeeds[_currentIndexPath]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //for test                
                multipartFormData.append("\(self._currentFeed._id)".data(using:String.Encoding.utf8)!, withName: "post_id")
                
                multipartFormData.append("\(AppDelegate.getUser()._id)".data(using:String.Encoding.utf8)!, withName: "user_id")                
                
        },
            to: Constants.REQ_SETSHARE,
            
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("set emoji like :", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                print(self._newsfeeds[self._currentIndexPath]._caption)
                                self._newsfeeds[self._currentIndexPath]._shareCount+=1
                                
                                self.cvNewsfeed.reloadData()
                                
                                //self.hideLoadingView()
                                self.showToast("sharing succeed")
                                
                            }
                            
                        } else  {
                            
                            self.showToast("sharing failed")
                            //self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    // MARK: collection view datasource
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return _newsfeeds.count
        return _newsfeeds.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let feed = _newsfeeds[indexPath.row]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsfeedCell", for: indexPath) as! NewsfeedCell
        
        cell.imvUserPhoto.sd_setImage(with: URL(string:feed._photo_url), placeholderImage: UIImage(named: "img_user"))
        cell.lblUserName.text = feed._user_name
        cell.lblQuestion1.text = feed._question1
        cell.lblQuestion2.text = feed._question2
        cell.tvCaption.text = feed._caption
        cell.imvPicture.sd_setImage(with: URL(string:feed._file_url), placeholderImage:UIImage(named : "ic_attach"))
        
        if feed._recentLikeType != -1 && feed._likeCount > 0 {
            
                cell.imvLike1.image = UIImage(named: Constants.like_icons[feed._recentLikeType])
                cell.imvLike1.frame.size.width = 20
                cell.imvLike1.isHidden = false
            
        } else {
            cell.imvLike1.frame.size.width = 0
            cell.imvLike1.isHidden = true
        }
    
        if feed._myLikeType != -1 && feed._likeCount > 0 {
            if feed._recentLikeType != feed._myLikeType {
                cell.imvLike2.image = UIImage(named: Constants.like_icons[feed._myLikeType])
                cell.imvLike2.frame.size.width = 20
                cell.imvLike2.isHidden = false
            } else {
                cell.imvLike2.frame.size.width = 0
                cell.imvLike2.isHidden = true
            }
        } else {
            cell.imvLike2.frame.size.width = 0
            cell.imvLike2.isHidden = true
        }
        
        if feed._likeCount != 0 {
                    if feed._myLikeType != -1 {
                        cell.lblLike.text = "You and " + String(feed._likeCount-1) + " others"
        
                    } else {
                cell.lblLike.text = String(feed._likeCount) + " likes"
                    }
        } else {
            cell.lblLike.text = "0 like"
        }
        
        if feed._commentCount == 0 {
            cell.lblComment.text = "0 comments"
        } else {
            cell.lblComment.text = String(feed._commentCount) + " comments"
        }
        
        if feed._shareCount == 0 {
            cell.lblShare.text = "0 shares"
        } else {
            cell.lblShare.text = String(feed._shareCount) + " shares"
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {        
        
        let feed = self._newsfeeds[indexPath.row];
        let photoVC = self.storyboard?.instantiateViewController(withIdentifier: "PhotoViewController") as! PhotoViewController
        photoVC._photoUrl = feed._file_url
        self.present(photoVC, animated: false, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        _currentIndexPath = Int(self.cvNewsfeed.contentOffset.x / self.cvNewsfeed.frame.size.width)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view .endEditing(true)
    }
}

extension NewsfeedViewController : UICollectionViewDelegateFlowLayout {


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: CGFloat(w), height: CGFloat(h))
    
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
