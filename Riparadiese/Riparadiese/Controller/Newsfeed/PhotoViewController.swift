//
//  PhotoViewController.swift
//  Riparadise
//
//  Created by JIS on 4/4/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import ImageScrollView
import SDWebImage

class PhotoViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var imgScrollView: ImageScrollView!
    
    var _photoUrl:String!

    var imageView:UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        detailViewed = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.initData()
    }
    
    func initData() {
        
        print(self.view.frame.size.width)
        print(self.view.frame.size.height)
        
        imageView = UIImageView.init(frame: CGRect(x:0, y:00, width:self.view.frame.size.width, height:self.view.frame.size.height))
        
        imageView.sd_setImage(with: URL(string:self._photoUrl), placeholderImage:UIImage(named : "ic_attach"))
        
        imgScrollView.display(image: imageView.image!)
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
}
