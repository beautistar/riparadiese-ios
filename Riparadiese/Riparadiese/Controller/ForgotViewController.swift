//
//  ForgotViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material
import Alamofire

class ForgotViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {        
        
//        let textFields : [TextField] = [tfEmail, tfPassword, tfConfirmPassword];
//        
//        for textField in textFields {
//            
//            textField.placeholderNormalColor = Color.grey.base
//            textField.placeholderActiveColor = Constants.mainColor
//            textField.dividerNormalColor = Color.grey.base
//            textField.dividerActiveColor = Constants.mainColor
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == tfEmail) {
            tfPassword.becomeFirstResponder()
        } else if textField == tfPassword {
            tfConfirmPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func resetAction(_ sender: Any) {
        
        if self.isValid() {
            
            self.resetPassword()
        }
    }
    
    func isValid() -> Bool {
        
        if tfEmail.text == nil || tfEmail.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if !isValidEmail(testStr: tfEmail.text!) {
            
            showAlertDialog(title: nil, message: R.string.INVALID_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfPassword.text == nil || (tfPassword.text?.characters.count)! < 6 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfConfirmPassword.text == nil || tfConfirmPassword.text != tfPassword.text {
            
            showAlertDialog(title: nil, message: R.string.CONFIRM_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
    }
    
    func resetPassword() {
        
        showLoadingView()
        
        let email = tfEmail.text!
        let password = tfPassword.text!
        
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(email)".data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append("\(password)".data(using:String.Encoding.utf8)!, withName: "password")
        },
            to: Constants.REQ_RESETPWD,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("reset pwd response : ", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let user = UserEntity()
                                user._password = password
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(user._password, forKey: Constants.PASSWORD)
                                
                                AppDelegate.setUser(user: user)
                                
                                self.backAction(_:self)
                                
                            } else if result_code == 204 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.USER_NOTEXIST, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    

}
