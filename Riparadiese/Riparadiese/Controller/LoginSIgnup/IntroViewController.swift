//
//  IntroViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/18/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material

class IntroViewController: BaseViewController {

//    @IBOutlet weak var lblTerm: UnderlinedLabel!
//    
//    @IBOutlet weak var imvCheck: UIImageView!
    
    var isChecked:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.initUI()        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        self.navigationController?.navigationBar.isHidden = true
        
//        let origImage_off = UIImage(named: "ic_check_off")
//        let tintedImage = origImage_off?.withRenderingMode(.alwaysTemplate)
//        
//        imvCheck.image = tintedImage
//        imvCheck.tintColor = Constants.mainColor
//        
//        lblTerm.text = "Terms & Privacy Policy"
//        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(IntroViewController.viewTerm(_:)))
//        lblTerm.addGestureRecognizer(tapGesture)
        
    }
 

    @IBAction func loginAction(_ sender: Any) {
        
    }
    
    @IBAction func signupAction(_ sender: Any) {
       
        let signupVC = self.storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
}

class UnderlinedLabel: UILabel {
    
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.characters.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSUnderlineStyleAttributeName , value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}
