//
//  SignupViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/18/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material
import Alamofire
import ActionSheetPicker

class SignupViewController: BaseViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imvProfile: UIImageView!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfConfirmPassword: UITextField!
    @IBOutlet weak var tfBirthday: UITextField!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var btnBirthday: UIButton!
    @IBOutlet weak var addPhotoButton: UIButton!
    @IBOutlet weak var lblTerms: UnderlinedLabel!
    @IBOutlet weak var imvCheck: UIImageView!
    
    let _picker: UIImagePickerController = UIImagePickerController()
    var _imgProfile : String?
    var isChecked:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.isHidden = false
        
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.handleTapView(_:)))
        self.view.addGestureRecognizer(tapper)
        
        self._picker.delegate = self
        _picker.allowsEditing = true

        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        _imgProfile = ""
        
        imvProfile.layer.borderColor = Constants.grayColor.cgColor
        imvProfile.layer.borderWidth = 1.0
        
        addPhotoButton.titleLabel?.textAlignment = NSTextAlignment.center
        if _imgProfile?.characters.count != 0 {
            addPhotoButton.titleLabel?.text = ""
        }
        
        let origImage_off = UIImage(named: "ic_check_off")
        let tintedImage = origImage_off?.withRenderingMode(.alwaysTemplate)

        imvCheck.image = tintedImage
        imvCheck.tintColor = Constants.mainColor

        lblTerms.text = "Terms & Privacy Policy"

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignupViewController.viewTerm(_:)))
        lblTerms.addGestureRecognizer(tapGesture)
    
    }
    

    @IBAction func checkAction(_ sender: Any) {
        
        if isChecked {

            let origImage_off = UIImage(named: "ic_check_off")
            let tintedImage = origImage_off?.withRenderingMode(.alwaysTemplate)

            imvCheck.image = tintedImage
            imvCheck.tintColor = Constants.mainColor
            

        } else {

            let origImage_on = UIImage(named: "ic_check_on")
            let tintedImage = origImage_on?.withRenderingMode(.alwaysTemplate)
            imvCheck.image = tintedImage
            imvCheck.tintColor = Constants.mainColor            
        }

        isChecked = !isChecked
    }

    func viewTerm(_ sender:Any){

        let termVC = self.storyboard?.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        self.navigationController?.pushViewController(termVC, animated: true)
    }
    
    
    func handleTapView(_ sender:UITapGestureRecognizer) {
        
        self.view.endEditing(true);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == tfUserName {
            tfEmail.becomeFirstResponder()
        }
        if (textField == tfEmail) {
            tfPassword.becomeFirstResponder()
        } else if textField == tfPassword {
            tfConfirmPassword.becomeFirstResponder()
        } else if textField == tfConfirmPassword {
            self.view.endEditing(true)
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    @IBAction func photoAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cameraAction: UIAlertAction = UIAlertAction(title: R.string.CAMERA, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(self._picker, animated: true, completion: nil)
            })
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in
                self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            photoSourceAlert.addAction(cameraAction)
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
        }
        else
        {
            _picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            let photoSourceAlert: UIAlertController = UIAlertController(title: R.string.PHOTO_SOURCE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let albumAction: UIAlertAction = UIAlertAction(title: R.string.PHOTO_ALBUMS, style: UIAlertActionStyle.default, handler: {
                (photoSourceAlert) -> Void in self._picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(self._picker, animated: true, completion: nil)
            })
            
            
            photoSourceAlert.addAction(albumAction)
            photoSourceAlert.addAction(UIAlertAction(title: R.string.CANCEL, style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(photoSourceAlert, animated: true, completion: nil);
            
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            
            self.imvProfile.image = pickedImage
            _imgProfile = saveToFile(image: pickedImage, filePath: Constants.SAVE_ROOT_PATH, fileName: "temp.png")
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        _imgProfile = ""
        
        dismiss(animated: true, completion: nil)
    }


    func isValid() -> Bool {
        
        if tfUserName.text == nil || tfUserName.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_USERNAME, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfEmail.text == nil || tfEmail.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if !isValidEmail(testStr: tfEmail.text!) {
            
            showAlertDialog(title: nil, message: R.string.INVALID_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfPassword.text == nil || (tfPassword.text?.characters.count)! < 6 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfConfirmPassword.text == nil || tfConfirmPassword.text != tfPassword.text {
            
            showAlertDialog(title: nil, message: R.string.CONFIRM_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfBirthday.text == nil || tfBirthday.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_BIRTHDAY, positive: R.string.OK, negative: nil)
            return false
        }
        
        if _imgProfile?.characters.count == 0 {
            showAlertDialog(title: nil, message: R.string.SELECT_PHOTO, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
        
    }
    
    @IBAction func signupAction(_ sender: Any) {
        
        if self.isValid() {
            
            self.doRegister()
        }
        
    }
    
    func doRegister() {
        
        showLoadingView()
        
        let username = tfUserName.text!
        let email = tfEmail.text!
        let password = tfPassword.text!
        let birthday = tfBirthday.text!
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(URL(fileURLWithPath:self._imgProfile!), withName: "file")
                multipartFormData.append("\(username)".data(using:String.Encoding.utf8)!, withName: "username")
                multipartFormData.append("\(email)".data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append("\(password)".data(using:String.Encoding.utf8)!, withName: "password")
                multipartFormData.append("\(birthday)".data(using:String.Encoding.utf8)!, withName: "birthday")
        },
            to: Constants.REQ_REGISTER,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("register response : ", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let user = UserEntity()
                                user._id = JSON[Constants.RES_ID] as! Int
                                user._email = email
                                user._username = username
                                user._password = password
                                user._birthday = birthday
                                user._profileUrl = JSON[Constants.RES_PHOTOURL] as! String
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(user._email, forKey:Constants.EMAIL)
                                defaults.setValue(user._password, forKey: Constants.PASSWORD)
                                
                                AppDelegate.setUser(user: user)
                                
                                self.gotoMain()
                                
                            } else if result_code == 201 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.USERENAME_EXIST, positive: R.string.OK, negative: nil)
                                
                            } else if result_code == 202 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.EMAIL_EXIST, positive: R.string.OK, negative: nil)
                            } else if result_code == 203 {
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.UPLOAD_FAIL, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    @IBAction func birthdayAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date!, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            
//            let dateString = value as! String
            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"

            dateFormatter.dateFormat = "dd MMM yyyy"
            dateFormatter.locale = Locale.init(identifier: "en_GB")
//            let dateObj = dateFormatter.date(from: dateString)
            
//            dateFormatter.dateFormat = "MM-dd-yyyy"
            print("Dateobj: \(dateFormatter.string(from: value! as! Date))")
            
            let dateStr = dateFormatter.string(from:value! as! Date)
//            let words = dateStr.components(separatedBy: " ")
            
            self.tfBirthday.text = dateStr
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin:btnBirthday)
        let secondsInHundYear: TimeInterval = 100 * 365 * 24 * 60 * 60;
        datePicker?.minimumDate = NSDate(timeInterval: -secondsInHundYear, since: NSDate() as Date) as Date!
        datePicker?.maximumDate = NSDate(timeInterval: 0, since: NSDate() as Date) as Date!
        
        datePicker?.show()
    }

    
    func gotoMain() {
        
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainTab = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! UITabBarController
        UIApplication.shared.keyWindow?.rootViewController = mainTab
        
    }

    
    @IBAction func backAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }

    
}
