//
//  LoginViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/18/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Material
import Alamofire

class LoginViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let tapper = UITapGestureRecognizer(target: self, action: #selector(self.handleTapView(_:)))
        self.view.addGestureRecognizer(tapper)
        
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initUI() {
        
        self.navigationController?.navigationBar.isHidden = false
        
//        let textFields : [TextField] = [tfEmail, tfPassword];
        
//        for textField in textFields {
//            
//            textField.placeholderNormalColor = Color.grey.base
//            textField.placeholderActiveColor = Constants.mainColor
//            textField.dividerNormalColor = Color.grey.base
//            textField.dividerActiveColor = Constants.mainColor
//        }
        
        //load from saved data
        let defaults = UserDefaults.standard
        
        let email = defaults.string(forKey: Constants.EMAIL)
        let pwd = defaults.string(forKey: Constants.PASSWORD)
        
        if email?.characters.count != 0 {
            
            tfEmail.text = email
            tfPassword.text = pwd
        }
    }
    
    func handleTapView(_ sender:UITapGestureRecognizer) {
        
        self.view.endEditing(true);
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == tfEmail) {
            tfPassword.becomeFirstResponder()
        }
        
        textField.resignFirstResponder()
        
        return true
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = navigationController?.popViewController(animated: true)
    }
    
    func isValid() -> Bool {
        
        if tfEmail.text == nil || tfEmail.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if !isValidEmail(testStr: tfEmail.text!) {
            
            showAlertDialog(title: nil, message: R.string.INVALID_EMAIL, positive: R.string.OK, negative: nil)
            return false
        }
        
        if tfPassword.text == nil || tfPassword.text?.characters.count == 0 {
            
            showAlertDialog(title: nil, message: R.string.INPUT_PWD, positive: R.string.OK, negative: nil)
            return false
        }
        
        return true
    }

    @IBAction func loginAction(_ sender: Any) {
        
        //for test
        if self.isValid() {
            
            self.doLogin()
        }
    }
    
    func doLogin() {
        
        showLoadingView()
        
        let email = tfEmail.text!
        let password = tfPassword.text!

        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append("\(email)".data(using:String.Encoding.utf8)!, withName: "email")
                multipartFormData.append("\(password)".data(using:String.Encoding.utf8)!, withName: "password")
        },
            to: Constants.REQ_LOGIN,
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                    
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        
                        print("login response : ", response)
                        
                        if let result = response.result.value {
                            
                            let JSON = result as! NSDictionary
                            
                            let result_code = JSON[Constants.RES_RESULTCODE] as! Int
                            
                            if result_code == Constants.CODE_SUCESS {
                                
                                self.hideLoadingView()
                                
                                let user = UserEntity()
                                user._id = Int(JSON[Constants.RES_ID] as! String)!
                                user._email = email
                                user._password = password
                                user._username = JSON[Constants.RES_USERNAME] as! String
                                user._birthday = JSON[Constants.RES_BIRTHDAY] as! String
                                user._profileUrl = JSON[Constants.RES_PHOTOURL] as! String
                                
                                let defaults = UserDefaults.standard
                                defaults.setValue(user._email, forKey:Constants.EMAIL)
                                defaults.setValue(user._password, forKey: Constants.PASSWORD)
                                
                                AppDelegate.setUser(user: user)
                                
                                self.gotoMain()
                                
                            } else if result_code == 204 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.USER_NOTEXIST, positive: R.string.OK, negative: nil)
                                
                            } else if result_code == 205 {
                                
                                self.hideLoadingView()
                                self.showAlertDialog(title: R.string.ERROR, message: R.string.WRONG_PWD, positive: R.string.OK, negative: nil)
                            }
                            
                        } else  {
                            
                            self.hideLoadingView()
                            self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                        }
                        
                    }
                    
                case .failure:
                    
                    self.showAlertDialog(title: R.string.ERROR, message: R.string.CONNECT_FAIL, positive: R.string.OK, negative: nil)
                    
                    self.hideLoadingView()
                    
                    return
                    
                }
        }
        )
    }
    
    func gotoMain() {
        
        let mainTab = self.storyboard?.instantiateViewController(withIdentifier: "MainTabVC") as! UITabBarController
        
        UIApplication.shared.keyWindow?.rootViewController = mainTab
    }
}
