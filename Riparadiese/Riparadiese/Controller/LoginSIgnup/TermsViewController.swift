//
//  TermsViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/18/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class TermsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
}
