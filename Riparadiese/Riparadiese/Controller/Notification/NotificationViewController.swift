//
//  NotificationViewController.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tblNotification: UITableView!
    
    var _notifications = [NewsfeedEntity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblNotification.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        _notifications = Newsfeeds

    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        return _assigns.count;
        return Newsfeeds.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        let newsfeed = Newsfeeds[indexPath.row]
        
        cell.imvUserPhoto.sd_setImage(with: URL(string:newsfeed._photo_url), placeholderImage: UIImage(named: "img_user"))
        cell.lblUserName.text = newsfeed._user_name + " posted new photo"
        
        return cell
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let newsfeed = Newsfeeds[indexPath.row]
        
        let profileVC = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController        
        
        
        //var newControllers = [UIViewController]()
        //newControllers += self.navigationController!.viewControllers
        
        //newControllers[newControllers.count - 1] = profileVC
        
        let owner = UserEntity()
        owner._id = newsfeed._user_id
        owner._username = newsfeed._user_name
        owner._profileUrl = newsfeed._photo_url
        profileVC._user = owner
        FromNotification = 1
        
        //self.navigationController?.setViewControllers(newControllers, animated: true)
        self.navigationController?.pushViewController(profileVC, animated: true)
        
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            
            Newsfeeds.remove(at: indexPath.row)
            self.tblNotification.deleteRows(at: [indexPath], with: .automatic)
            
        }
    }
    
}
