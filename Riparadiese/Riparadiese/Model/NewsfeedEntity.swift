//
//  NewsfeedEntity.swift
//  Riparadise
//
//  Created by JIS on 3/29/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

class NewsfeedEntity {
    
    var _id = 0
    var _user_id = 0
    var _user_name = ""
    var _photo_url = ""
    var _caption = ""
    var _question1 = ""
    var _question2 = ""
    var _file_url = ""
    var _time = ""
    
    var _commentCount = 0
    var _shareCount = 0
    var _likeCount = 0
    var _myLikeType = 0
    var _recentLikeType = 0
    
}
