//
//  NSObject+Extension.swift
//  Riparadiese
//
//  Created by JIS on 3/19/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation
import UIKit

extension NSObject {
    
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last! as String
    }
    
    //    class var identifier: String {
    //        return String(format: "%@_identifier", self.nameOfClass)
    //    }
}
